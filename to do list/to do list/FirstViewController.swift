//
//  FirstViewController.swift
//  to do list
//
//  Created by Click Labs 65 on 1/20/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

var toDoItems : [String] = []  //to do array declared globally acessible to both view controllers

class FirstViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var toDoTable: UITableView! //link to table
    
    override func viewDidLoad() {
        super.viewDidLoad() // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning() // Dispose of any resources that can be recreated.
    }
    
    //below functions update tableview with rows
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoItems.count  //returns number of rows to be filled
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default , reuseIdentifier: "cell")
        cell.textLabel?.text = toDoItems[indexPath.row]
        return cell
    }// above function sets label of the rows
    
    override func viewWillAppear(animated: Bool) {   //function updates the table view everytime bar button is pressed
        if var storedToDo: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("toDoItems") {
            toDoItems = []
            for var  i = 0 ; i < storedToDo.count ; ++i {
                toDoItems.append(storedToDo[i] as NSString)  // appends into array along with stored todos
            }
        }
        toDoTable.reloadData()
    } // everytime when the list tab is clicked the table is reloaded with data
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if ( editingStyle == UITableViewCellEditingStyle.Delete) {
            toDoItems.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            
            //below code is used to update values in array declared
            
            let fixedToDoItems = toDoItems
            NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey: "toDoItems")
            NSUserDefaults.standardUserDefaults().synchronize()
        }  //whenever a item is deleted this function is called along with it also updates the array
    }
}

