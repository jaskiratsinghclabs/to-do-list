//
//  SecondViewController.swift
//  to do list
//
//  Created by Click Labs 65 on 1/20/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var addField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    
    @IBAction func addButton(sender: AnyObject) {
        if addField.text != "" && addField.text != "Add here"{
            self.view.endEditing(true)
            toDoItems.append(addField.text)
            
            let fixedToDoItems = toDoItems // mutable variable for storing of todos
            NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey: "toDoItems") // persistence storage setting up key
            NSUserDefaults.standardUserDefaults().synchronize()
            addField.text = ""   //on button press item is stored into the persistent storage defined above
        }else{
            addField.text = "Add here"
        }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }  // for keyboard to appear/disappear when external touches take place
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        addField.resignFirstResponder()
        return true
    } //  return button is pressed keyboard disappear

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

